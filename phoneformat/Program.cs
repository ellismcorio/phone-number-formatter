﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace phoneformat
{
    class Program
    {
        static void Main(string[] args)
        {
            // [1, 2, 3, 4, 5, 6, 7, 8, 9, 0] ➞ "(123) 456-7890"
            // [5, 1, 9, 5, 5, 5, 4, 4, 6, 8] ➞ "(519) 555-4468"
            // [3, 4, 5, 5, 0, 1, 2, 5, 2, 7] ➞ "(345) 501-2527"

            var number1 = new[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 };
            var number2 = new[] { 5, 1, 9, 5, 5, 5, 4, 4, 6, 8 };
            var number3 = new[] { 3, 4, 5, 5, 0, 1, 2, 5, 2, 7 };


            /////////////////////////////////////////////////////////////////////////////////////////////////
            //////////////// swap out {number2} on line 29 & 35 to test the other arrays ////////////////////
            /////////////////////////////////////////////////////////////////////////////////////////////////

            while (true) {

                string output = "(";
                for (int i = 0; i < number2.Length; i++)
                {
                    output += number2[i];

                    if (i == 2)
                    {
                        output += ") ";
                    }
                    if (i == 5)
                    {
                        output += "-";
                    }
                }
                Console.WriteLine(output);
                
              
                Console.WriteLine();

                string line = Console.ReadLine();
                if (line == "exit") // Check string
                {
                    break;
                }

            }
        }
    }
}
